package ru.t1.azarin.tm.repository;

import ru.t1.azarin.tm.api.repository.IUserOwnedRepository;
import ru.t1.azarin.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public M add(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) return null;
        model.setUserId(userId);
        return add(model);
    }

    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        models.clear();
    }

    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        final List<M> result = new ArrayList<>();
        for (final M model : models) {
            if (userId.equals(model.getUserId())) result.add(model);
        }
        return result;
    }

    public List<M> findAll(final String userId, final Comparator comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    public M findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        for (final M model : models) {
            if (!userId.equals(model.getUserId())) continue;
            if (!id.equals(model.getId())) continue;
            return model;
        }
        return null;
    }

    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    public boolean existById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    public M remove(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) return null;
        if (model == null) return null;
        return removeById(userId, model.getId());
    }

    public M removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    public M removeByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

}
