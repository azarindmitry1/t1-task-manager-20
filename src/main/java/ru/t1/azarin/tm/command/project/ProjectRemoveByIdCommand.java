package ru.t1.azarin.tm.command.project;

import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    public final static String NAME = "project-remove-by-id";

    public final static String DESCRIPTION = "Remove project by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        serviceLocator.getProjectTaskService().removeProjectById(userId, project.getId());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
