package ru.t1.azarin.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public final static String NAME = "project-clear";

    public final static String DESCRIPTION = "Clear all projects.";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = getUserId();
        serviceLocator.getProjectService().clear(userId);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
