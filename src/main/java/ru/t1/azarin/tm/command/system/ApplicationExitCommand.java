package ru.t1.azarin.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    public final static String NAME = "exit";

    public final static String DESCRIPTION = "Close application.";

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
