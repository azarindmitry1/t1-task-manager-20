package ru.t1.azarin.tm.command.project;

import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    public final static String NAME = "project-start-by-id";

    public final static String DESCRIPTION = "Start project by id.";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        serviceLocator.getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
