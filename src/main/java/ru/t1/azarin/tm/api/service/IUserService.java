package ru.t1.azarin.tm.api.service;

import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.User;

public interface IUserService extends IService<User> {

    User removeByLogin(String login);

    User removeByEmail(String email);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User setPassword(String id, String password);

    User updateUser(
            String id,
            String firstName,
            String middleName,
            String lastName
    );

}
