package ru.t1.azarin.tm.api.repository;

import ru.t1.azarin.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void clear();

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    boolean existById(String id);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

}
