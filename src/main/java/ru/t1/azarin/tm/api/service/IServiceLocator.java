package ru.t1.azarin.tm.api.service;

public interface IServiceLocator {

    ICommandService getCommandService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    ILoggerService getLoggerService();

    IUserService getUserService();

    IAuthService getAuthService();

}
