package ru.t1.azarin.tm.api.model;

public interface ICommand {

    void execute();

    String getName();

    String getArgument();

    String getDescription();

}
